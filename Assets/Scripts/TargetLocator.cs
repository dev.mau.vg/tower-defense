using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLocator : MonoBehaviour
{
    [SerializeField] Transform weapon;
    [SerializeField] ParticleSystem proyectileParticles;
    [SerializeField] float towerRange = 15f;
    Transform target;

    void Update()
    {
        FindClosestTarget();
        AimWeapon();        
    }

    void FindClosestTarget()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        Transform closestTarget = null;

        float maxDistance = Mathf.Infinity;

        foreach (Enemy enemy in enemies)
        {
            float targetDistance = Vector3.Distance(transform.position, enemy.transform.position);

            if (targetDistance < maxDistance)
            {
                maxDistance = targetDistance;
                closestTarget = enemy.transform;
            }
        }

        target = closestTarget;
    }

    void AimWeapon()
    {
        if (target == null)
        {
            return;
        }

        float targetDistance = Vector3.Distance(transform.position, target.position);

        weapon.LookAt(target);

        Attack(targetDistance < towerRange);
    }

    void Attack(bool isActive)
    {
        var emissionModule = proyectileParticles.emission;
        emissionModule.enabled = isActive;
    }
}
