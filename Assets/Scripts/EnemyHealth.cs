using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyHealth : MonoBehaviour
{
    [SerializeField] float maxHitPoints = 5f;
    Enemy enemy;
    float health;

    // Start is called before the first frame update
    void OnEnable()
    {
        health = maxHitPoints;
    }

    private void Start()
    {
        enemy = GetComponent<Enemy>();
    }

    private void OnParticleCollision(GameObject other)
    {
        if (--health < 1)
        {
            gameObject.SetActive(false);
            GetComponent<EnemyMover>().movementSpeed += 0.05f;
            health = maxHitPoints;
            maxHitPoints += 0.5f;
            enemy.RewardGold();
        }
    }
}
