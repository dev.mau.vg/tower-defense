using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NodeClass
{
    public Vector2Int coordinates;
    public bool isWalkable;
    public bool isExplored;
    public bool isPath;
    public NodeClass conectedTo;

    public NodeClass(Vector2Int inCoordinates, bool inIsWalkable)
    {
        this.coordinates = inCoordinates;
        this.isWalkable = inIsWalkable;
    }
}
