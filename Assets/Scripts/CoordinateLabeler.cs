using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using TMPro;

[ExecuteAlways]
[RequireComponent(typeof(TextMeshPro))]
public class CoordinateLabeler : MonoBehaviour
{
    Color defaultColor = Color.white;
    Color blockedColor = Color.grey;
    Color exploredColor = Color.blue;
    Color pathColor = Color.yellow;

    TextMeshPro label;
    Vector2Int coordinates = new Vector2Int();

    GridManager gridManager;

    private void Awake()
    {
        label = GetComponent<TextMeshPro>();
        label.enabled = false;
        gridManager = FindObjectOfType<GridManager>();
        DisplayCoordinates();
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            DisplayCoordinates();
            UpdateObjectName();
        }

        SetLabelColor();
        DisplayLabelText();
    }

    void SetLabelColor()
    {
        if (gridManager == null) { return; }

        NodeClass node = gridManager.GetNode(coordinates);

        if (node == null) { return; }

        if (!node.isWalkable)
        {
            label.color = blockedColor;
        }

        else if (node.isPath)
        {
            label.color = pathColor;
        }

        else if (node.isExplored)
        {
            label.color = exploredColor;
        }
        
        else
        {
            label.color = defaultColor;
        }    
    }

    void DisplayLabelText()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            label.enabled = !label.IsActive();
        }
    }

    void DisplayCoordinates()
    {
        if (gridManager == null)
        {
            return;
        }

        Vector3 parentPosition = transform.parent.position;
        
        coordinates.x = Mathf.RoundToInt(parentPosition.x / gridManager.UnityGridSize);
        coordinates.y = Mathf.RoundToInt(parentPosition.z / gridManager.UnityGridSize);
        label.text = coordinates.ToString();
    }

    void UpdateObjectName()
    {
        transform.parent.name = coordinates.ToString();
    }
}
