using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class PathFinder : MonoBehaviour
{

    [SerializeField] private Vector2Int startCoordinates;
    public Vector2Int StartCoordinates { get { return startCoordinates; } }
    [SerializeField] private Vector2Int destinationCoordinates;
    public Vector2Int DestinationCoordinates { get { return destinationCoordinates; } }

    NodeClass _startNode;
    NodeClass _destinationNode;
    NodeClass _currentSearchNode;
    
    Vector2Int[] _directions = { Vector2Int.right,  Vector2Int.left, Vector2Int.up, Vector2Int.down };
    GridManager _gridManager;

    Queue<NodeClass> _frontier = new Queue<NodeClass>();

    Dictionary<Vector2Int, NodeClass> _grid = new Dictionary<Vector2Int, NodeClass>();
    Dictionary<Vector2Int, NodeClass> _reached = new Dictionary<Vector2Int, NodeClass>();

    void Awake()
    {
        _gridManager = FindObjectOfType<GridManager>();

        if (_gridManager != null)
        {
            _grid = _gridManager.Grid;
            
            _startNode = _grid[startCoordinates];
            _destinationNode = _grid[destinationCoordinates];
        }
    }

    void Start()
    {
        GetNewPath();
    }

    public List<NodeClass> GetNewPath()
    {
        return GetNewPath(StartCoordinates);
    }
    
    public List<NodeClass> GetNewPath(Vector2Int coord)
    {
        _gridManager.ResetNodes();
        BreadthFirstSearch(coord);
        return BuildPath();
    }

    void ExploreNeighbors()
    {
        List<NodeClass> neighbors = new List<NodeClass>();

        foreach (Vector2Int direction in _directions)
        {
            Vector2Int neighborCoords = _currentSearchNode.coordinates + direction;

            if (_grid.ContainsKey(neighborCoords))
            {
                neighbors.Add(_grid[neighborCoords]);
            }
        }

        foreach (NodeClass neighbor in neighbors)
        {
            if (!_reached.ContainsKey(neighbor.coordinates) && neighbor.isWalkable)
            {
                neighbor.conectedTo = _currentSearchNode;
                _reached.Add(neighbor.coordinates, neighbor);
                _frontier.Enqueue(neighbor);
            }
        }
    }

    void BreadthFirstSearch(Vector2Int coordinates)
    {
        _startNode.isWalkable = true;
        _destinationNode.isWalkable = true;
        
        _frontier.Clear();
        _reached.Clear();
        
        bool isRunning = true;
        
        _frontier.Enqueue(_grid[coordinates]);
        _reached.Add(coordinates, _grid[coordinates]);

        while (_frontier.Count > 0 && isRunning)
        {
            _currentSearchNode = _frontier.Dequeue();
            _currentSearchNode.isExplored = true;
            ExploreNeighbors();

            if (_currentSearchNode.coordinates == destinationCoordinates)
            {
                isRunning = false;
            }
        }
    }

    List<NodeClass> BuildPath()
    {
        List<NodeClass> path = new List<NodeClass>();
        NodeClass currentNode = _destinationNode;
        
        path.Add(currentNode);
        currentNode.isPath = true;

        while (currentNode.conectedTo != null)
        {
            currentNode = currentNode.conectedTo;
            path.Add(currentNode);
            currentNode.isPath = true;
        }
        
        path.Reverse();

        return path;
    }

    public bool WillBlockPath(Vector2Int coord)
    {
        if (_grid.ContainsKey(coord))
        {
            bool state = _grid[coord].isWalkable;
            
            _grid[coord].isWalkable = false;
            List<NodeClass> newPath = GetNewPath();
            _grid[coord].isWalkable = state;

            if (newPath.Count <= 1)
            {
                GetNewPath();
                return true;
            }
        }

        return false;
    }

    public void NotifyReceivers()
    {
        BroadcastMessage("RecalculatePath", false, SendMessageOptions.DontRequireReceiver);
    }
}
