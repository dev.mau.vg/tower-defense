using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Bank : MonoBehaviour
{
    [SerializeField] int StartingBalance = 150;
    [SerializeField] TextMeshProUGUI displayBalance;

    [SerializeField] int currentBalance;
    public int CurrentBalance { get { return currentBalance; } }



    private void Awake()
    {
        currentBalance = StartingBalance;
        UpdateDisplay();
    }

    public void Deposit (int Amount)
    {
        currentBalance += Mathf.Abs(Amount);
        UpdateDisplay();
    }

    public bool Withdraw(int Amount)
    {
        if (Amount <= currentBalance)
        {
            currentBalance -= Mathf.Abs(Amount);
            UpdateDisplay();

            return true;
        }

        ReloadScene();
        return false;
    }

    void ReloadScene()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }

    void UpdateDisplay()
    {
        displayBalance.text = "Gold: " + currentBalance;
    }
}
